### Visualisation

#### Start
```
catkin_make
source /opt/ros/melodic/setup.sh
source devel/setup.sh
```


#### Update robot description
```
rosrun xacro xacro src/ros_basics_2019/urdf/robot_description.urdf.xacro > src/ros_basics_2019/urdf/robot_description.urdf
rosparam set robot_description "`cat src/ros_basics_2019/urdf/robot_description.urdf`"
```

#### Verify robot description
```
check_urdf <(xacro --inorder src/ros_basics_2019/urdf/robot_description.urdf.xacro)
```

#### Generate image and view
```
urdf_to_graphiz src/ros_basics_2019/urdf/robot_description.urdf.xacro
evince SpesBot.pdf
```

#### References
http://wiki.ros.org/urdf/Tutorials/Using%20Xacro%20to%20Clean%20Up%20a%20URDF%20File