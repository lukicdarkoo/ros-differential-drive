<?xml version="1.0"?>
<robot xmlns:xacro="https://www.ros.org/wiki/xacro" name="SpesBot">

    <xacro:include filename="$(find ros_basics_2019)/urdf/macros.xacro" />
    <xacro:include filename="$(find ros_basics_2019)/urdf/materials.xacro" />

    <xacro:property name="width" value="0.11" />
    <xacro:property name="length" value="0.112" />
    <xacro:property name="height" value="0.045" />


    <xacro:property name="body_mass" value="${0.27 * 0.8}" />
    <xacro:property name="wheel_mass" value="${0.27 * 0.1}" />

    <xacro:property name="wheel_thickness" value="0.015" />
    <xacro:property name="wheel_radius" value="0.022" />

    <xacro:property name="sensor_size" value="0.01" />

    <!-- Design your robot here -->
    <!-- 
    <link name="base_link"></link>

    <joint name="robot_footprint_joint" type="fixed">
        <origin xyz="0 0 0" rpy="0 0 0" />
        <parent link="base_link"/>
        <child link="chassis" />
    </joint>
    -->

    <link name="base_link">
        <inertial>
            <mass value="${body_mass}"/>
            <xacro:box_inertia m="${body_mass}" x="${length}" y="${width}" z="${height}" />
        </inertial>

        <collision name='collision'>
            <origin xyz="0 0 ${0.045 - 2 * 0.009}" />
            <geometry>
                <box size="${length} ${width} ${height}"/>
            </geometry>
        </collision>

        <visual name='chassis_visual'>
            <origin xyz="0 0 ${0.045 - 2 * 0.009}" />
            <geometry>
                <box size="${length} ${width} ${height}"/>
            </geometry>
            <material name="white"/>
        </visual>


        <!-- Cylinder -->
        <collision name='collision_cylinder'>
            <origin xyz="${length/2.5} 0 0.005" />
            <geometry>
                <sphere radius="0.009"/>
            </geometry>
        </collision>

        <visual name='cylinder'>
            <origin xyz="${length/2.5} 0 0.005" />
            <geometry>
                <sphere radius="0.009"/>
            </geometry>
            <material name="red"/>
        </visual>
    </link>


    <joint name="footprint_left_sensor" type="fixed">
        <origin xyz="${length/2} ${width/2} ${height/2}" />
        <parent link="base_link"/>
        <child link="left_sensor_link" />
        <axis xyz="0.0 1 0.0"/>
    </joint>

    <link name="left_sensor_link">
        <visual name="left_sensor_visual">
            <origin xyz="0 0 0" />
            <geometry>
                <box size="${sensor_size} ${sensor_size} ${sensor_size}"/>
            </geometry>
            <material name="red"/>
        </visual>

        <collision name='left_sensor_collision'>
            <origin xyz="0 0 0" />
            <geometry>
                <box size="${sensor_size} ${sensor_size} ${sensor_size}"/>
            </geometry>
        </collision>
    </link>

    <joint name="footprint_right_sensor" type="fixed">
        <origin xyz="${length/2} ${-width/2} ${height/2}" />
        <parent link="base_link"/>
        <child link="right_sensor_link" />
        <axis xyz="0.0 1 0.0"/>
    </joint>

    <link name="right_sensor_link">
        <visual name="right_sensor_visual">
            <origin xyz="0 0 0" />
            <geometry>
                <box size="${sensor_size} ${sensor_size} ${sensor_size}"/>
            </geometry>
            <material name="red"/>
        </visual>
    </link>


    <link name="left_wheel">
        <inertial>
            <mass value="${wheel_mass}"/>
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <xacro:box_inertia m="${body_mass}" x="${length}" y="${width}" z="${height}" />
        </inertial>
        <collision name="left_wheel_collision">
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="${wheel_thickness}" radius="${wheel_radius}"/>
            </geometry>
        </collision>
        <visual name="left_wheel_visual">
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="${wheel_thickness}" radius="${wheel_radius}"/>
            </geometry>
            <material name="black" />
        </visual>
    </link>

    <link name="right_wheel">
        <inertial>
            <mass value="${wheel_mass}"/>
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <xacro:box_inertia m="${body_mass}" x="${length}" y="${width}" z="${height}" />
        </inertial>
        <collision name="left_wheel_collision">
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="${wheel_thickness}" radius="${wheel_radius}"/>
            </geometry>
        </collision>
        <visual name="left_wheel_visual">
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="${wheel_thickness}" radius="${wheel_radius}"/>
            </geometry>
            <material name="black" />
        </visual>
    </link>

    <joint name="footprint_left_wheel" type="continuous">
        <origin xyz="-${width/6} ${wheel_thickness/2.1-width/2} ${height/2-0.005}" rpy="0 0 0" />
        <parent link="base_link"/>
        <child link="left_wheel" />
        <axis xyz="0.0 1 0.0"/>
    </joint>

    <joint name="footprint_right_wheel" type="continuous">
        <origin xyz="-${width/6} ${-wheel_thickness/2.1+width/2} ${height/2-0.005}" rpy="0 0 0" />
        <parent link="base_link"/>
        <child link="right_wheel" />
        <axis xyz="0.0 1 0.0"/>
    </joint>

    <gazebo>
        <plugin name="differential_drive_controller" filename="libgazebo_ros_diff_drive.so">
            <alwaysOn>true</alwaysOn>
            <updateRate>10</updateRate>
            <leftJoint>footprint_left_wheel</leftJoint>
            <rightJoint>footprint_right_wheel</rightJoint>
            <wheelSeparation>${width - wheel_thickness}</wheelSeparation>
            <wheelDiameter>${wheel_radius}</wheelDiameter>

            <commandTopic>cmd_vel</commandTopic>
            <odometryTopic>odom</odometryTopic>
            <odometryFrame>odom</odometryFrame>

            <robotBaseFrame>base_link</robotBaseFrame>
        </plugin>
    </gazebo>

    <gazebo reference="left_sensor_link">
        <sensor type="ray" name="left_sensor">
            <pose>0 0 0 0 0 0</pose>
            <ray>
                <scan>
                    <horizontal>
                        <samples>1</samples>
                        <resolution>1</resolution>
                        <min_angle>0.5</min_angle>
                        <max_angle>0.5</max_angle>
                    </horizontal>
                </scan>
                <range>
                    <min>0.01</min>
                    <max>2.0</max>
                    <resolution>0.01</resolution>
                </range>
                <noise>
                    <type>gaussian</type>
                    <mean>0.0</mean>
                    <stddev>0.01</stddev>
                </noise>
            </ray>
            <plugin name="laser" filename="libgazebo_ros_laser.so">
                <topicName>leftsensor/scan</topicName>
                <frameName>left_sensor_link</frameName>
            </plugin>
            <always_on>1</always_on>
            <update_rate>10</update_rate>
            <visualize>true</visualize>
        </sensor>
    </gazebo>

    <gazebo reference="right_sensor_link">
        <sensor type="ray" name="right_sensor">
            <pose>0 0 0 0 0 0</pose>
            <ray>
                <scan>
                    <horizontal>
                        <samples>1</samples>
                        <resolution>1</resolution>
                        <min_angle>-0.5</min_angle>
                        <max_angle>-0.5</max_angle>
                    </horizontal>
                </scan>
                <range>
                    <min>0.01</min>
                    <max>2.0</max>
                    <resolution>0.01</resolution>
                </range>
                <noise>
                    <type>gaussian</type>
                    <mean>0.0</mean>
                    <stddev>0.01</stddev>
                </noise>
            </ray>
            <plugin name="laser" filename="libgazebo_ros_laser.so">
                <topicName>rightsensor/scan</topicName>
                <frameName>right_sensor_link</frameName>
            </plugin>
            <always_on>1</always_on>
            <update_rate>10</update_rate>
            <visualize>true</visualize>
        </sensor>
    </gazebo>
</robot>