
    <joint name="footprint_left_wheel" type="fixed">
        <origin xyz="0 ${-width/2} 0" rpy="0 0 0" />
        <parent link="base_link"/>
        <child link="left_wheel" />
    </joint>

    <joint name="footprint_right_wheel" type="fixed">
        <origin xyz="0 ${width/2} 0" rpy="0 0 0" />
        <parent link="base_link"/>
        <child link="right_wheel" />
    </joint>

    <link name="left_wheel">
        <inertial>
            <mass value="5.0"/>
            <origin xyz="0.0 0 0" rpy="0 ${pi/2} ${pi/2}"/>
            <inertia ixx="0.1" ixy="0" ixz="0" iyy="0.1" iyz="0" izz="0.1" />
        </inertial>
        <collision name="left_wheel_collision">
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="0.05" radius="0.1"/>
            </geometry>
        </collision>
        <visual name="left_wheel_visual">
            <origin xyz="0 ${-width/2} 0" rpy="0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="0.02" radius="0.05"/>
            </geometry>
            <material name="black"/>
        </visual>
    </link>

    <link name="right_wheel">
        <inertial>
            <mass value="5.0"/>
            <origin xyz="0.0 0 0" rpy="0 ${pi/2} ${pi/2}"/>
            <inertia ixx="0.1" ixy="0" ixz="0" iyy="0.1" iyz="0" izz="0.1" />
        </inertial>
        <collision name="right_wheel_collision">
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="0.05" radius="0.1"/>
            </geometry>
        </collision>
        <visual name="right_wheel_visual">
            <origin xyz="0 ${width/2} 0" rpy="0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="0.02" radius="0.05"/>
            </geometry>
            <material name="black"/>
        </visual>
    </link>

    <!-- Below you will find samples of gazebo plugins you may want to use. -->
    <!-- These should be adapted to your robot's design -->
    <gazebo reference="YOUR_SENSOR_LINK">
        <sensor type="ray" name="laser_right">
            <pose>0 0 0 0 0 0</pose>
            <ray>
                <scan>
                    <horizontal>
                        <samples>13</samples>
                        <resolution>1</resolution>
                        <min_angle>-1.571</min_angle>
                        <max_angle>1.571</max_angle>
                    </horizontal>
                </scan>
                <range>
                    <!-- You can edit adapt these to your robot's size -->
                    <min>0.0005</min>
                    <max>0.04</max>
                    <resolution>0.0001</resolution>
                </range>
            </ray>
            <plugin name="laser" filename="libgazebo_ros_laser.so">
                <topicName>YOUR_ROBOT_LASER/scan</topicName>
                <frameName>YOUR_ROBOT_LASER_LINK</frameName>
            </plugin>
            <always_on>1</always_on>
            <update_rate>10</update_rate>
            <visualize>true</visualize>
        </sensor>
    </gazebo>

    <gazebo>
        <plugin name="differential_drive_controller" filename="libgazebo_ros_diff_drive.so">
            <alwaysOn>true</alwaysOn>
            <updateRate>20</updateRate>
            <leftJoint>YOUR_LEFT_WHEEL_JOINT</leftJoint>
            <rightJoint>YOUR_RIGHT_WHEEL_JOINT</rightJoint>
            <wheelSeparation>XXX</wheelSeparation>
            <wheelDiameter>YYY</wheelDiameter>
            <wheelTorque>ZZZ</wheelTorque>

            <commandTopic>cmd_vel</commandTopic>
            <odometryTopic>odom</odometryTopic>
            <odometryFrame>odom</odometryFrame>

            <robotBaseFrame>YOUR_BASE_FRAME</robotBaseFrame>
        </plugin>
    </gazebo>


















    <?xml version="1.0"?>

<robot xmlns:xacro="https://www.ros.org/wiki/xacro" name="SpesBot">

    <xacro:include filename="$(find ros_basics_2019)/urdf/macros.xacro" />
    <xacro:include filename="$(find ros_basics_2019)/urdf/materials.xacro" />

    <xacro:property name="width" value="0.11" />
    <xacro:property name="length" value="0.112" />
    <xacro:property name="height" value="0.045" />

    
    <xacro:property name="body_mass" value="${0.27 * 0.8}" />
    <xacro:property name="wheel_mass" value="${0.27 * 0.1}" />

    <xacro:property name="wheel_thickness" value="0.015" />
    <xacro:property name="wheel_radius" value="0.022" />


    <!-- Design your robot here -->
    <!-- 
    <link name="base_link"></link>

    <joint name="robot_footprint_joint" type="fixed">
        <origin xyz="0 0 0" rpy="0 0 0" />
        <parent link="base_link"/>
        <child link="chassis" />
    </joint>
    -->

    <link name="base_link">
        <inertial>
            <mass value="${body_mass}"/>
            <xacro:box_inertia m="${body_mass}" x="${length}" y="${width}" z="${height}" />
        </inertial>

        <collision name='collision'>
            <origin xyz="0 0 ${0.045 - 2 * 0.009}" />
            <geometry>
                <box size="${length} ${width} ${height}"/>
            </geometry>
        </collision>

        <visual name='chassis_visual'>
            <origin xyz="0 0 ${0.045 - 2 * 0.009}" />
            <geometry>
                <box size="${length} ${width} ${height}"/>
            </geometry>
            <material name="white"/>
        </visual>

       

        <collision name='collision_cylinder'>
            <origin xyz="${-length/2.5} 0 0.005" />
            <geometry>
                <sphere radius="0.009"/>
            </geometry> 
            
        </collision>

        <visual name='cylinder'>
            <origin xyz="${-length/2.5} 0 0.005" />
            <geometry>
                <sphere radius="0.009"/>
            </geometry> 
            
            <material name="red"/>
        </visual>
    </link>

    <joint name="footprint_left_wheel" type="continuous">
        <origin xyz="0 ${-width/2} 0" rpy="0 0 0" />
        <parent link="base_link"/>
        <child link="left_wheel" />
        <axis xyz="0.0 1 0.0"/>
    </joint>

    <joint name="footprint_right_wheel" type="continuous">
        <origin xyz="0 ${width/2} 0" rpy="0 0 0" />
        <parent link="base_link"/>
        <child link="right_wheel" />
        <axis xyz="0.0 1 0.0"/>
    </joint>

    <link name="left_wheel">
        <inertial>
            <mass value="${wheel_mass}"/>
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <xacro:box_inertia m="${body_mass}" x="${length}" y="${width}" z="${height}" />
        </inertial>
        <collision name="left_wheel_collision">
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="${wheel_thickness}" radius="${wheel_radius}"/>
            </geometry>
        </collision>
        <visual name="left_wheel_visual">
            <origin xyz="0 0 0" rpy=" 0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="${wheel_thickness}" radius="${wheel_radius}"/>
            </geometry>
            <material name="black" />
        </visual>
    </link>

    <link name="right_wheel">
        <inertial>
            <mass value="${wheel_mass}"/>
            <origin xyz="${width/6} ${-wheel_thickness/2.1}  ${height/2-0.005}" rpy=" 0 ${pi/2} ${pi/2}"/>
            <xacro:box_inertia m="${body_mass}" x="${length}" y="${width}" z="${height}" />
        </inertial>
        <collision name="left_wheel_collision">
            <origin xyz="${width/6} ${-wheel_thickness/2.1} ${height/2-0.005}" rpy=" 0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="${wheel_thickness}" radius="${wheel_radius}"/>
            </geometry>
        </collision>
        <visual name="left_wheel_visual">
            <origin xyz="${width/6} ${-wheel_thickness/2.1} ${height/2-0.005}" rpy=" 0 ${pi/2} ${pi/2}"/>
            <geometry>
                <cylinder length="${wheel_thickness}" radius="${wheel_radius}"/>
            </geometry>
            <material name="black" />
        </visual>    
    </link>

    

    <gazebo>
        <plugin name="differential_drive_controller" filename="libgazebo_ros_diff_drive.so">
            <alwaysOn>true</alwaysOn>
            <updateRate>20</updateRate>
            <leftJoint>footprint_left_wheel</leftJoint>
            <rightJoint>footprint_right_wheel</rightJoint>
            <wheelSeparation>${width - wheel_thickness}</wheelSeparation>
            <wheelDiameter>${wheel_radius}</wheelDiameter>

            <commandTopic>cmd_vel</commandTopic>
            <odometryTopic>odom</odometryTopic>
            <odometryFrame>odom</odometryFrame>

            <robotBaseFrame>base_link</robotBaseFrame>
        </plugin>
    </gazebo>
</robot>
