#!/usr/bin/python

"""
$ rosrun ros_basics_2019 control.py

$ rostopic pub /cmd_vel geometry_msgs/Twist "linear:
  x: 0.1
  y: 0.0
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 0.0"
"""


import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Point, Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan
from time import sleep
import signal
import sys
import tf
import numpy as np


ANGLE_P = 1
ANGLE_D = 0.1
POS_P = 1
POS_D = 0.1
ACC_SLOPE = 0.01
ROTATION_EPS = 0.02
ROTATION_TRANS_EPS = 0.1
DISTANCE_EPS = 0.05
SAFE_REGION = 0.1
TOP_SPEED = 0.08

STATE_ANGLE = 0
STATE_FORWARD = 1
STATE_STOP = 2
STATE_AVOIDANCE = 3
STATE_ESCAPE = 4

odom = Odometry()
goal = Point()
state = STATE_FORWARD
range_left = np.inf
range_right = np.inf

pub_scan = rospy.Publisher('scan', LaserScan, queue_size=1)

def signal_handler(sig, frame):
    sys.exit(0)

def goal_pos_cb(pose):
    """
    $ rostopic pub /goal_pos geometry_msgs/Point "x: 5
y: 0
z: 0"
    """
    global goal
    global state
    goal = pose
    state = STATE_ANGLE
    print(f'Goal received: {goal}')
    rospy.loginfo(pose)

def odom_cb(pose):
    global odom
    odom = pose
    """
    $ rostopic pub /odom nav_msgs/Odometry "???
    """
    # rospy.loginfo(pose)

def laser_left_cb(laser):
    pub_scan.publish(laser)
    global range_left
    range_left = laser.ranges[0]

def laser_right_cb(laser):
    global range_right
    range_right = laser.ranges[0]

def get_euler(odom):
    quaternion = (
        odom.pose.pose.orientation.x,
        odom.pose.pose.orientation.y,
        odom.pose.pose.orientation.z,
        odom.pose.pose.orientation.w
    )
    return tf.transformations.euler_from_quaternion(quaternion)


def get_pose(odom):
    return np.array([
        odom.pose.pose.position.x,
        odom.pose.pose.position.y,
        odom.pose.pose.position.z
    ])


def print_status(target_angle, target_pose, current_pose, euler):
    if False:
        return

    print(f'State {state}')
    print(f'[Angle] Target: {np.round(target_angle, 2)}, current {np.round(euler[2], 2)}, diff {np.round(target_angle - euler[2], 2)}')
    print(f'[Position] Target: {np.round(target_pose, 2)}, current {np.round(current_pose, 2)}, diff {np.round(target_pose - current_pose, 2)}')


def listener():
    global state

    prev_pos_err = 0
    prev_angle_err = 0
    prev_speed = 0
    avoiding_counter = 0

    pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)

    rospy.init_node('control', anonymous=True)
    rospy.Subscriber('goal_pos', Point, goal_pos_cb)
    rospy.Subscriber('odom', Odometry, odom_cb)
    rospy.Subscriber('leftsensor/scan', LaserScan, laser_left_cb)
    rospy.Subscriber('rightsensor/scan', LaserScan, laser_right_cb)

    

    while True:
        target_pos = np.array([goal.x, goal.y, goal.z])
        target = Twist()

        euler = get_euler(odom)
        current_pose = get_pose(odom)

        diff = target_pos - current_pose
        target_angle = np.arctan2(diff[1], diff[0])

        # Calculate angle error
        angle_err = (euler[2] - target_angle)
        if angle_err > np.pi:
            angle_err -= 2 * np.pi
        elif angle_err < -np.pi:
            angle_err += 2 * np.pi

        if state == STATE_ANGLE:
            # PD controller
            target.angular.z = angle_err * ANGLE_P
            target.angular.z += (angle_err - prev_angle_err) * ANGLE_D
            prev_angle_err = angle_err

            # Change state
            if range_right < SAFE_REGION or range_left < SAFE_REGION:
                target.linear.x = 0
                state = STATE_AVOIDANCE   
            elif abs(target_angle - euler[2]) > ROTATION_EPS:
                state = STATE_ANGLE
            elif np.linalg.norm(diff) > DISTANCE_EPS:
                state = STATE_FORWARD

        elif state == STATE_FORWARD:
            pos_err = np.linalg.norm(target_pos - current_pose)

            # PD controller
            target.angular.z = 0
            target.linear.x = pos_err * POS_P
            target.linear.x += (pos_err - prev_pos_err) * POS_D
            prev_pos_err = pos_err

            # Slope (throttle the acceleration)
            if target.linear.x - prev_speed > ACC_SLOPE:
                target.linear.x = prev_speed + ACC_SLOPE
            prev_speed = target.linear.x 

            if target.linear.x > TOP_SPEED:
                target.linear.x = TOP_SPEED

            # Change state
            if range_right < SAFE_REGION or range_left < SAFE_REGION:
                target.linear.x = 0
                state = STATE_AVOIDANCE  
            elif np.linalg.norm(diff) < DISTANCE_EPS:
                prev_speed = 0
                state = STATE_STOP
            elif abs(target_angle - euler[2]) > ROTATION_TRANS_EPS:
                prev_speed = 0
                state = STATE_ANGLE
                          
        elif state == STATE_STOP:
            # Turn off the motors
            target.linear.x = 0
            target.angular.z = 0

            # Change state
            if np.linalg.norm(diff) > 0.1:
                state = STATE_FORWARD

        elif state == STATE_AVOIDANCE:
            target.angular.z = 0.2 if range_right > range_left else -0.2

            # Change state
            if range_right > SAFE_REGION * 2 and range_left > SAFE_REGION * 2:
                avoiding_counter = 0
                prev_speed = 0
                state = STATE_ESCAPE
                    
        elif state == STATE_ESCAPE:
            target.linear.x = 0.1
            target.angular.z = 0

            # Slope (throttle the acceleration)
            if target.linear.x - prev_speed > ACC_SLOPE:
                target.linear.x = prev_speed + ACC_SLOPE
            prev_speed = target.linear.x

            avoiding_counter += 1

            # Change state
            if range_right < SAFE_REGION * 2 or range_left < SAFE_REGION * 2:
                target.linear.x = 0
                state = STATE_AVOIDANCE
                avoiding_counter = 0
            elif avoiding_counter > 30:
                state = STATE_ANGLE
            

        # print_status(target_angle, target_pos, current_pose, euler)
        pub.publish(target)
        sleep(0.1)

    rospy.spin()

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)

    listener()
